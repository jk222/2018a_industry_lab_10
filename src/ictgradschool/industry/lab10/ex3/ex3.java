package ictgradschool.industry.lab10.ex3;

import com.sun.javafx.image.BytePixelSetter;
import ictgradschool.industry.lab10.ex02.ExerciseTwo;

public class ex3 {

    private int foo(int x) {
        if (x <= 1) {
            return 1;


        }
        return x * foo(x - 1);

    }


    private double bar(double x, int n) {
        if (n > 1) {
            double i = bar(x, n - 1);
            return x * i;
        } else if (n < 0) {
            double i = bar(x, -n);
            return 1.0 / i;
        } else {
            return x;
        }

    }

    private void bad1() {
        System.out.println("AAAAA");
        bad1();
    }

    private int bad2(int n) {
        if (n == 0) {
            return 0;
        }
        return n + bad2(n - 2);
    }


    private int bad3(int n) {
        if (n == 0) {
            return 0;
        }
        return n + bad3(n + 1);


    }

    private void start() {
        //int AnswerX = foo(4);

        // double answerY= bar(2,3);
        // System.out.println(answerY);
        //double answerZ= bar(3,-2);
        //System.out.println(answerZ);

        //int answerZZ= bad2(7);
        //System.out.println(answerZZ);

        int answerYY = bad3(1);

        System.out.println(answerYY);

    }

    public static void main(String[] args) {
        new ex3().start();
    }

}
